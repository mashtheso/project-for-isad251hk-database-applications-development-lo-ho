﻿CREATE TABLE [dbo].[Orders] (
    [OrderID]     INT IDENTITY (1, 1) NOT NULL,
    [DrinkID]     INT NOT NULL,
    [DrinkAmount] INT NOT NULL,
    [UserID]      INT NOT NULL,
    PRIMARY KEY CLUSTERED ([OrderID] ASC),
    CONSTRAINT [FK_Orders_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);

