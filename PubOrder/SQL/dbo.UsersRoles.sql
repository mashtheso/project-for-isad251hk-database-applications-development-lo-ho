﻿CREATE TABLE [dbo].[UsersRoles] (
    [UsersRolesID] INT IDENTITY (1, 1) NOT NULL,
    [UserID]       INT NOT NULL,
    [RoleID]       INT NOT NULL,
    PRIMARY KEY CLUSTERED ([UsersRolesID] ASC),
    CONSTRAINT [FK_UsersRoles_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);

