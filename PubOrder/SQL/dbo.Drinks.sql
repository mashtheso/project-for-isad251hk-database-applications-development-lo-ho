﻿CREATE TABLE [dbo].[Drinks] (
    [DrinkID]          INT           IDENTITY (1, 1) NOT NULL,
    [DrinkName]        VARCHAR (254) NOT NULL,
    [DrinkPrice]       FLOAT (53)    NOT NULL,
    [DrinkSize]        VARCHAR (50)  NOT NULL,
    [DrinkPicturePath] VARCHAR (254) NOT NULL,
    PRIMARY KEY CLUSTERED ([DrinkID] ASC)
);

