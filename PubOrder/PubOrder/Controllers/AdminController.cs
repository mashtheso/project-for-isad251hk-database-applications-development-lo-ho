﻿using PubOrder.Models;
using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PubOrder.Controllers
{
    public class AdminController : Controller
    {
        #region // Access Denied Action
        [HttpGet]
        public ActionResult AccessDenied()
        {
            return View();
        }
        #endregion

        #region // Admin Panel Action
        [Authorize]
        [HttpGet]
        public ActionResult AdminPanel()
        {
            using (DBEntities de = new DBEntities())
            {
                User user = de.Users.Where(a => a.EmailID == HttpContext.User.Identity.Name).FirstOrDefault();
                if (user != null)
                {
                    var userRoles = user.UsersRoles;
                    foreach (var userRole in userRoles)
                    {
                        if (userRole.Role.RoleName.Equals("Admin"))
                        {
                            if (userRole.UserID == user.UserID)
                            {
                                return View();
                            }
                        }
                    }
                }
            }

            return RedirectToAction("AccessDenied", "Admin");
        }
        #endregion

        #region // Upload Drink Action
        [Authorize]
        [HttpGet]
        public ActionResult UploadDrink()
        {
            using (DBEntities de = new DBEntities())
            {
                User user = de.Users.Where(a => a.EmailID == HttpContext.User.Identity.Name).FirstOrDefault();
                if (user != null)
                {
                    var userRoles = user.UsersRoles;
                    foreach (var userRole in userRoles)
                    {
                        if (userRole.Role.RoleName.Equals("Admin"))
                        {
                            if (userRole.UserID == user.UserID)
                            {
                                return View();
                            }
                        }
                    }
                }
            }

            return RedirectToAction("AccessDenied", "Admin");
        }
        #endregion

        #region // Upload Drink POST Action
        [Authorize]
        [HttpPost]
        public ActionResult UploadDrink(Drink drink, FormCollection formCollection, HttpPostedFileBase Image)
        {
            string message = "";
            using (DBEntities de = new DBEntities())
            {
                User user = de.Users.Where(a => a.EmailID == HttpContext.User.Identity.Name).FirstOrDefault();
                if (user != null)
                {
                    if (user.IsEmailVerified != true)
                    {
                        ViewBag.Message = "You cannot upload drinks until you verify your account!";
                        return View();
                    }
                    else
                    {
                        var userRoles = user.UsersRoles;
                        foreach (var userRole in userRoles)
                        {
                            if (userRole.Role.RoleName.Equals("Admin"))
                            {
                                if (userRole.UserID == user.UserID)
                                {
                                    bool canUpload = false;

                                    string ext = Path.GetExtension(Image.FileName);
                                    if ((ext.Equals(".xbm")) || (ext.Equals(".bmp")) || (ext.Equals(".jpeg")) || (ext.Equals(".webp")) || (ext.Equals(".svgz")) || (ext.Equals(".gif")) || (ext.Equals(".jfif")) || (ext.Equals(".png")) || (ext.Equals(".svg")) || (ext.Equals(".jpg")) || (ext.Equals(".ico")) || (ext.Equals(".tiff")) || (ext.Equals(".pjpeg")) || (ext.Equals(".pjp")) || (ext.Equals(".tif")))
                                    {

                                        drink.DrinkPicturePath = ""; 

                                        if (de.Drinks.Count() < 1)
                                        {
                                            canUpload = true;
                                        }
                                        else
                                        {
                                            foreach (var item in de.Drinks)
                                            {
                                                if (item.DrinkName.Equals(drink.DrinkName))
                                                {
                                                    canUpload = false;
                                                    break;
                                                }
                                                else
                                                {
                                                    canUpload = true;
                                                }
                                            }
                                        }

                                        if (canUpload == true)
                                        {
                                            if (ModelState.IsValid)
                                            {
                                                drink.DrinkSize = "1";


                                                de.Drinks.Add(drink);
                                                de.SaveChanges();
                                                string fileName = Path.GetFileNameWithoutExtension(Image.FileName);
                                                string extension = Path.GetExtension(Image.FileName);
                                                int drinkIndex = drink.DrinkID;
                                                drink.DrinkPicturePath = "/Uploads/" + drinkIndex + extension;
                                                Image.SaveAs(Server.MapPath(drink.DrinkPicturePath));
                                                de.SaveChanges();
                                                message = "Drink uploaded successfully!";
                                            }
                                            else
                                            {
                                                message = "Invalid Request!";
                                            }
                                        }
                                        else
                                        {
                                            message = "This drink is already present in the database!";
                                        }
                                    }
                                    else
                                    {
                                        message = "The selected image is not of a supported file format! " + ext;
                                    }
                                }
                            }
                        }
                    }

                    ViewBag.Message = message;
                    return View();
                }
            }

            return RedirectToAction("AccessDenied", "Admin");
        }
        #endregion

        #region // Manage Users Action
        [Authorize]
        [HttpGet]
        public ActionResult ManageUsers()
        {
            using (DBEntities de = new DBEntities())
            {
                User user = de.Users.Where(a => a.EmailID == HttpContext.User.Identity.Name).FirstOrDefault();
                if (user != null)
                {
                    var userRoles = user.UsersRoles;
                    foreach (var userRole in userRoles)
                    {
                        if (userRole.Role.RoleName.Equals("Admin"))
                        {
                            if (userRole.UserID == user.UserID)
                            {
                                var allUsers = de.Users.ToList();
                                ViewBag.Message = TempData["giveAdminText"];
                                ViewBag.Message2 = TempData["revokeAdminText"];
                                ViewBag.Message3 = TempData["deleteUserText"];
                                return View(allUsers);
                            }
                        }
                    }
                }
            }

            return RedirectToAction("AccessDenied", "Admin");
        }
        #endregion

        #region // Give Admin POST Action
        [Authorize]
        [HttpPost]
        public ActionResult GiveAdmin(FormCollection formCollection)
        {
            using (DBEntities de = new DBEntities())
            {
                User user = de.Users.Where(a => a.EmailID == HttpContext.User.Identity.Name).FirstOrDefault();
                if (user != null)
                {
                    if (user.IsEmailVerified != true)
                    {
                        TempData["giveAdminText"] = "You cannot give admin access until you verify your account!";
                        return RedirectToAction("ManageUsers", "Admin");
                    }
                    else
                    {
                        var userRoles = user.UsersRoles;
                        foreach (var userRole in userRoles)
                        {
                            if (userRole.Role.RoleName.Equals("Admin"))
                            {
                                if (userRole.UserID == user.UserID)
                                {
                                    if (formCollection["UsersList"] != "")
                                    {
                                        string userEmail = formCollection["UsersList"].ToString();
                                        User selectedUser = de.Users.Where(a => a.EmailID == userEmail).FirstOrDefault();
                                        if (selectedUser != null)
                                        {
                                            var selectedUserRole = selectedUser.UsersRoles.FirstOrDefault();
                                            Role role = de.Roles.Where(a => a.RoleName.Equals("Admin")).FirstOrDefault();
                                            selectedUserRole.User = selectedUser;
                                            selectedUserRole.Role = role;
                                            selectedUser.UsersRoles.Add(selectedUserRole);
                                            de.SaveChanges();
                                            TempData["giveAdminText"] = "User with email '" + userEmail + "' was given admin access!";
                                        }
                                    }
                                    else
                                    {
                                        TempData["giveAdminText"] = "Please select a user to which to give admin access to first!";
                                    }
                                }
                            }
                        }
                    }

                    return RedirectToAction("ManageUsers", "Admin");
                }
            }

            return RedirectToAction("AccessDenied", "Admin");
        }
        #endregion

        #region // Revoke Admin POST Action
        [Authorize]
        [HttpPost]
        public ActionResult RevokeAdmin(FormCollection formCollection)
        {
            using (DBEntities de = new DBEntities())
            {
                User user = de.Users.Where(a => a.EmailID == HttpContext.User.Identity.Name).FirstOrDefault();
                if (user != null)
                {
                    if (user.IsEmailVerified != true)
                    {
                        TempData["revokeAdminText"] = "You cannot revoke admin access until you verify your account!";
                        return RedirectToAction("ManageUsers", "Admin");
                    }
                    else
                    {
                        var userRoles = user.UsersRoles;
                        foreach (var userRole in userRoles)
                        {
                            if (userRole.Role.RoleName.Equals("Admin"))
                            {
                                if (userRole.UserID == user.UserID)
                                {
                                    if (formCollection["UsersList"] != "")
                                    {
                                        string userEmail = formCollection["UsersList"].ToString();
                                        User selectedUser = de.Users.Where(a => a.EmailID == userEmail).FirstOrDefault();
                                        if (selectedUser != null)
                                        {
                                            var selectedUserRole = selectedUser.UsersRoles.FirstOrDefault();
                                            Role role = de.Roles.Where(a => a.RoleName.Equals("User")).FirstOrDefault();
                                            selectedUserRole.User = selectedUser;
                                            selectedUserRole.Role = role;
                                            selectedUser.UsersRoles.Add(selectedUserRole);
                                            de.SaveChanges();
                                            TempData["revokeAdminText"] = "User with email '" + userEmail + "' was revoked admin access!";
                                        }
                                    }
                                    else
                                    {
                                        TempData["revokeAdminText"] = "Please select a user from which to revoke admin access first!";
                                    }
                                }
                            }
                        }
                    }

                    return RedirectToAction("ManageUsers", "Admin");
                }
            }

            return RedirectToAction("AccessDenied", "Admin");
        }
        #endregion

        #region // Delete User POST Action
        [Authorize]
        [HttpPost]
        public ActionResult DeleteUser(FormCollection formCollection)
        {
            using (DBEntities de = new DBEntities())
            {
                User user = de.Users.Where(a => a.EmailID == HttpContext.User.Identity.Name).FirstOrDefault();
                if (user != null)
                {
                    if (user.IsEmailVerified != true)
                    {
                        TempData["deleteUserText"] = "You cannot delete users until you verify your account!";
                        return RedirectToAction("ManageUsers", "Admin");
                    }
                    else
                    {
                        var userRoles = user.UsersRoles;
                        foreach (var userRole in userRoles)
                        {
                            if (userRole.Role.RoleName.Equals("Admin"))
                            {
                                if (userRole.UserID == user.UserID)
                                {
                                    if (formCollection["UsersList"] != "")
                                    {
                                        string userEmail = formCollection["UsersList"].ToString();
                                        User selectedUser = de.Users.Where(a => a.EmailID == userEmail).FirstOrDefault();
                                        if (selectedUser != null)
                                        {
                                            var userOrders = de.Orders.Where(a => a.UserID == selectedUser.UserID);
                                            foreach (var order in userOrders)
                                            {
                                                if (order != null)
                                                {
                                                    de.Orders.Remove(order);
                                                }
                                            }
                                            var userRoleToDelete = de.UsersRoles.Where(a => a.UserID == selectedUser.UserID).FirstOrDefault();
                                            de.UsersRoles.Remove(userRoleToDelete);
                                            de.Users.Remove(selectedUser);
                                            de.SaveChanges();
                                            TempData["deleteUserText"] = "User with email '" + userEmail + "' was deleted!";
                                        }
                                    }
                                    else
                                    {
                                        TempData["deleteUserText"] = "Please select a user which to delete first!";
                                    }
                                }
                            }
                        }
                    }

                    return RedirectToAction("ManageUsers", "Admin");
                }
            }

            return RedirectToAction("AccessDenied", "Admin");
        }
        #endregion

        #region // Delete Drink
        [Authorize]
        [HttpGet]
        public ActionResult DeleteDrink()
        {
            using (DBEntities de = new DBEntities())
            {
                User user = de.Users.Where(a => a.EmailID == HttpContext.User.Identity.Name).FirstOrDefault();
                if (user != null)
                {
                    var userRoles = user.UsersRoles;
                    foreach (var userRole in userRoles)
                    {
                        if (userRole.Role.RoleName.Equals("Admin"))
                        {
                            if (userRole.UserID == user.UserID)
                            {
                                return View();
                            }
                        }
                    }
                }
            }

            return RedirectToAction("AccessDenied", "Admin");
        }
        #endregion

        #region // Delete Drink POST Action
        [Authorize]
        [HttpPost]
        public ActionResult DeleteDrink(FormCollection formCollection)
        {
            string message = "";
            using (DBEntities de = new DBEntities())
            {
                User user = de.Users.Where(a => a.EmailID == HttpContext.User.Identity.Name).FirstOrDefault();
                if (user != null)
                {
                    if (user.IsEmailVerified != true)
                    {
                        message = "You cannot delete drinks until you verify your account!";
                    }
                    else

                    {
                        var userRoles = user.UsersRoles;
                        foreach (var userRole in userRoles)
                        {
                            if (userRole.Role.RoleName.Equals("Admin"))
                            {
                                if (userRole.UserID == user.UserID)
                                {
                                    if (formCollection["DrinksList"] != "")
                                    {
                                        string[] drinkNameAndSize = formCollection["DrinksList"].ToString().Split(new string[] { " - " }, StringSplitOptions.None);
                                        string drinkName = drinkNameAndSize[0];
                                        string drinkSize = drinkNameAndSize[1];
                                        var drink = de.Drinks.Where(a => (a.DrinkName == drinkName)).FirstOrDefault();
                                        if (drink != null)
                                        {
                                            int drinkID = drink.DrinkID;
                                            var ordersWithDrink = de.Orders.Where(a => a.DrinkID == drinkID);
                                            string imagePath = drink.DrinkPicturePath;
                                            if (System.IO.File.Exists(Request.MapPath(imagePath)))
                                            {
                                                foreach (var order in ordersWithDrink)
                                                {
                                                    if (order.DrinkID == drink.DrinkID)
                                                    {
                                                        de.Orders.Remove(order);
                                                    }
                                                }
                                                de.Drinks.Remove(drink);
                                                de.SaveChanges();
                                                System.IO.File.Delete(Request.MapPath(imagePath));
                                                message = "Drink '" + drink.DrinkName + " - " + "' successfully deleted!";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        message = "Please select a drink from the list first!";
                                    }
                                }
                            }
                        }
                    }

                    ViewBag.Message = message;
                    return View();
                }
            }

            return RedirectToAction("AccessDenied", "Admin");
        }
        #endregion
    }
}