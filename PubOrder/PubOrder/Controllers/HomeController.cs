﻿using PubOrder.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PubOrder.Controllers
{
    public class HomeController : Controller
    {
        #region // Index Action
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region // About Action
        [HttpGet]
        public ActionResult About()
        {
            return View();
        }
        #endregion

        #region // Show Drinks Action
        [HttpGet]
        public ActionResult ShowDrinks()
        {
            string message = "";
            List<Drink> drinksList = new List<Drink>();
            using (DBEntities de = new DBEntities())
            {
                if (de.Drinks.Count() > 0)
                {
                    foreach (var drink in de.Drinks)
                    {
                        if (drink != null)
                        {
                            drinksList.Add(drink);
                        }
                    }
                }
            }

            ViewBag.Message = message;
            return View(drinksList);
        }
        #endregion

        #region // Create Order With Drink Action
        [Authorize]
        [HttpGet]
        public ActionResult CreateOrderWithDrink(int id)
        {
            string message = "";
            bool canUploadNew = false;
            using (DBEntities de = new DBEntities())
            {
                var currentUserEmailIdentity = HttpContext.User.Identity.Name;
                var currentUser = de.Users.Where(a => a.EmailID == currentUserEmailIdentity).FirstOrDefault();
                if (currentUser.IsEmailVerified != true)
                {
                    message = "You cannot order anything until you verify your account!";
                }
                else
                {
                    var drink = de.Drinks.Where(a => a.DrinkID == id).FirstOrDefault();
                    if (drink != null)
                    {
                        if (de.Orders.Count() > 0)
                        {
                            foreach (var ordr in de.Orders)
                            {
                                if (ordr != null)
                                {
                                    if ((ordr.Drink.DrinkName.Equals(drink.DrinkName)))
                                    {
                                        ordr.DrinkAmount += 1;
                                        message = "Drink order successfully updated!";
                                        canUploadNew = false;
                                        break;
                                    }
                                    else
                                    {
                                        canUploadNew = true;
                                    }
                                }
                            }
                            de.SaveChanges();
                            if (canUploadNew == true)
                            {
                                Order order = new Order();
                                order.Drink = drink;
                                if (currentUser != null)
                                {
                                    order.User = currentUser;
                                }
                                order.DrinkAmount = 1;
                                de.Orders.Add(order);
                                message = "Drink successfully ordered!";
                                de.SaveChanges();
                            }
                        }
                        else
                        {
                            Order order = new Order();
                            order.Drink = drink;
                            if (currentUser != null)
                            {
                                order.User = currentUser;
                            }
                            order.DrinkAmount = 1;
                            de.Orders.Add(order);
                            message = "Drink successfully ordered!";
                            de.SaveChanges();
                        }
                    }
                }
            }

            TempData["showDrinksOrderInfo"] = message;
            return RedirectToAction("ShowDrinks", "Home");
        }
        #endregion

        #region // Checkout Action
        [Authorize]
        [HttpGet]
        public ActionResult Checkout()
        {
            string message = "";
            using (DBEntities de = new DBEntities())
            {
                List<Order> ordersList = new List<Order>();
                double totalOrderPrice = 0.0;
                var currentUserEmailIdentity = HttpContext.User.Identity.Name;
                var currentUser = de.Users.Where(a => a.EmailID == currentUserEmailIdentity).FirstOrDefault();

                var currentUserRole = de.UsersRoles.Where(a => a.UserID == currentUser.UserID).FirstOrDefault();



                var ordersByCurrentUser = new List<Order>();

                if (currentUserRole.RoleID == 1)
                {
                    ordersByCurrentUser = de.Orders.ToList();
                }
                else
                {
                    ordersByCurrentUser = de.Orders.Where(a => a.UserID == currentUser.UserID).ToList();
                }

                foreach (var order in ordersByCurrentUser)
                {
                    ordersList.Add(order);

                    if (!ViewData.ContainsKey("User" + order.OrderID))
                    {
                        ViewData.Add("User" + order.OrderID, order.UserID);
                    }



                    if (!ViewData.ContainsKey("drinkPicturePath" + order.OrderID))
                    {
                        ViewData.Add("drinkPicturePath" + order.OrderID, order.Drink.DrinkPicturePath);
                    }

                    if (!ViewData.ContainsKey("drinkName" + order.OrderID))
                    {
                        ViewData.Add("drinkName" + order.OrderID, order.Drink.DrinkName);
                    }

                    if (!ViewData.ContainsKey("drinSize" + order.OrderID))
                    {
                        ViewData.Add("drinSize" + order.OrderID, order.Drink.DrinkSize);
                    }

                    if (!ViewData.ContainsKey("orderPrice" + order.OrderID))
                    {
                        ViewData.Add("orderPrice" + order.OrderID, order.Drink.DrinkPrice * order.DrinkAmount);
                    }
                    totalOrderPrice += order.Drink.DrinkPrice * order.DrinkAmount;
                }

                if (!ViewData.ContainsKey("totalOrderPrice"))
                {
                    ViewData.Add("totalOrderPrice", totalOrderPrice);
                }

                if (ordersByCurrentUser.Count() == 1)
                {
                    message = "There is " + ordersByCurrentUser.Count().ToString() + " drinks in your cart!";
                }
                else if (ordersByCurrentUser.Count() > 1)
                {
                    message = "There are " + ordersByCurrentUser.Count().ToString() + " drinks in your cart!";
                }
                else
                {
                    message = "Your cart is empty!";
                }

                ViewBag.Message = message;
                ViewBag.Message2 = TempData["orderInfo"];
                return View(ordersList);
            }
        }
        #endregion

        #region // Add Order Action
        [Authorize]
        [HttpGet]
        public ActionResult AddOrder(int id)
        {
            string message = "";
            using (DBEntities de = new DBEntities())
            {
                User currentUser = de.Users.Where(a => a.EmailID == HttpContext.User.Identity.Name).FirstOrDefault();
                var order = de.Orders.Where(a => a.OrderID == id).FirstOrDefault();
                if (order != null)
                {
                    message = "Order #" + order.OrderID + " was successfully updated!";
                    order.DrinkAmount += 1;
                    de.SaveChanges();
                }

            }

            TempData["orderInfo"] = message;
            return RedirectToAction("Checkout", "Home");
        }
        #endregion

        #region // Remove Order Action
        [Authorize]
        [HttpGet]
        public ActionResult RemoveOrder(int id)
        {
            string message = "";
            using (DBEntities de = new DBEntities())
            {
                User currentUser = de.Users.Where(a => a.EmailID == HttpContext.User.Identity.Name).FirstOrDefault();
                if (currentUser.IsEmailVerified != true)
                {
                    message = "You cannot remove your orders until you verify your account!";
                }
                else
                {
                    var order = de.Orders.Where(a => a.OrderID == id).FirstOrDefault();
                    if (order != null)
                    {
                        if (order.DrinkAmount > 1)
                        {
                            message = "Order #" + order.OrderID + " was successfully updated!";
                            order.DrinkAmount -= 1;
                        }
                        else
                        {
                            var drink = order.Drink;
                            if (drink != null)
                            {
                                if (drink.DrinkName.Contains(""))
                                {
                                    de.Drinks.Remove(drink);

                                }
                            }
                            message = "Order #" + order.OrderID + " was successfully removed!";
                            de.Orders.Remove(order);
                        }
                        de.SaveChanges();
                    }
                }
            }

            TempData["orderInfo"] = message;
            return RedirectToAction("Checkout", "Home");
        }
        #endregion

        #region // Checkout Completed Action
        [Authorize]
        [HttpGet]
        public ActionResult CheckoutCompleted()
        {
            string message = "";
            using (DBEntities de = new DBEntities())
            {
                var currentUserEmailIdentity = HttpContext.User.Identity.Name;
                var currentUser = de.Users.Where(a => a.EmailID == currentUserEmailIdentity).FirstOrDefault();
                if (currentUser.IsEmailVerified != true)
                {
                    message = "You cannot checkout until you verify your account!";
                }
                else
                {
                    var ordersByCurrentUser = de.Orders.Where(a => a.UserID == currentUser.UserID);
                    if (ordersByCurrentUser.Count() > 0)
                    {
                        foreach (var order in ordersByCurrentUser)
                        {
                            if (order != null)
                            {
                                de.Orders.Remove(order);
                            }
                        }
                        message = "Your orders are on their way to your address at " + currentUser.Address + "!";
                        de.SaveChanges();
                    }
                    else
                    {
                        message = "You have not placed any orders yet!";
                    }
                }
            }

            ViewBag.Message = message;
            return View();
        }
        #endregion




    }
}